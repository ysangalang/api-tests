package au.com.axcelerate.apitest.sample


import static io.restassured.RestAssured.*
import static org.hamcrest.Matchers.*

import org.testng.annotations.Test

import au.com.axcelerate.apitest.PropertiesSupplier

class SampleTest {

    PropertiesSupplier props = new PropertiesSupplier()

    @Test
    void testGetConfig() {
        println props.getURL()
        println props.getToken()
    }

    @Test
    void testRestAssured() {
        when()
                .get("https://pokeapi.co/api/v2/pokemon/1")
        .then()
                .statusCode(200)
                .body("name", equalTo("bulbasaur"))
                .body("types[0].type.name", equalTo("grass"))
                .body("types[1].type.name", equalTo("poison"))

    }

}