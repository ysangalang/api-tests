package au.com.axcelerate.apitest

class PropertiesSupplier {

    private static final String env = System.getProperty("env")

    private static final Properties p = new Properties()

    PropertiesSupplier() {
        new File("src/main/resources/config.properties").withInputStream {
            p.load(it)
        }
    }

    String getURL() {
        p."${env ?: p.env}.url"
    }

    String getToken() {
        p."${env ?: p.env}.token"
    }

}
