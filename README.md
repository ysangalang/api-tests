# API Tests Runner
A project using Groovy and RESTAssured for testing aXcelerate's various REST-ful APIs

## Executions

+ Run all tests
    - `mvn test`
    - `mvn clean test` (also re-compiles sources)

+ Run specific test
    - `mvn test -Dtest=TestClassName`
    - `mvn test -Dtest=au.com.axcelerate.apitest.sample.TestClassName`

+ Run specific test method
    - `mvn test -Dtest=TestClassName#testMethodName`
    - `mvn test -Dtest=au.com.axcelerate.apitest.sample.TestClassName#testMethodName`

+ Run tests against a specific environment (see `config.properties`)
    - `mvn test -Denv=tst`
    - `mvn test -Denv=stg`
    - `mvn test -Denv=admin`